document.addEventListener("DOMContentLoaded", function () {

	// Mobile menu
	toggleMenu()


	// Glide js скрипты 
	new Glide('#top_promo_banner', {
		type: 'carousel',
		focusAt: 'center',
		autoplay: 5000
	}).mount()

	new Glide('#brands-slider_mobile', {
		type: 'carousel',
		autoplay: 2500,
		animationTimingFunc: 'ease',
		animationDuration: '700',
		perTouch: false,
		peek: {
			before: 10,
			after: 10
		},
		breakpoints: {
			1024: {
				perView: 11
			},
			768: {
				perView: 9
			},
			426: {
				perView: 5
			},
			359: {
				perView: 4
			},
		}
	}).mount()

	new Glide('#brands-slider_desktop', {
		type: 'carousel',
		perView: 10,
		autoplay: 4000,
		animationTimingFunc: 'ease',
		animationDuration: '500',
		peek: {
			before: 0,
			after: -40
		}
	}).mount()

	new Glide('#features-slider', {
		type: 'carousel',
		perView: 1,
		autoplay: 4000,
		// animationDuration: 400,
		hoverpause: false,
		breakpoints: {
			768: {
				perView: 2
			},
			600: {
				perView: 1
			}
		}
	}).mount()

	new Glide('#help-slider', {
		type: 'slider',
		perView: 4,
		gap: '12px',
		perTouch: 1,
		breakpoints: {
			1024: {
				perView: 3
			},
			560: {
				perView: 2,
			},
			425: {
				perView: 2,
				type: 'carousel',
				peek: {
					before: -80,
					after: 0
				}
			},
			359: {
				peek: {
					before: 0,
					after: 0
				}
			}
		}

	}).mount()

});

// Mobile menu
function toggleMenu() {
	var mobileMenu = document.getElementById("mobileMenu");
	if (mobileMenu.style.top === "-560px") {
		mobileMenu.style.top = "57px";
	} else {
		mobileMenu.style.top = "-560px";
	}
}

// Accodrion toggle
function accodrionToggle() {
	var accordionDefault = document.getElementById("ac-iccon--default");
	var accordionActive = document.getElementById("ac-iccon--active");


	if (accordionDefault.style.display === "block") {
		accordionDefault.style.display = "none";
		accordionActive.style.display = "block";
	} else {
		accordionDefault.style.display = "block";
		accordionActive.style.display = "none";
	}
}